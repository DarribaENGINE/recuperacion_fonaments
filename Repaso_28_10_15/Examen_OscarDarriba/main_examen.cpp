#include <stdio.h>
#include <cstdlib>
#include <string>
using namespace std;

void exercici1() {
	string nombre = "David";
	string nick = "Stylo";
	string adress = "Passeig de gracia 156";
	int maxScore = 49538;
	bool isBanned = true;
	int numMissionsAccomplished = 19;
	char inicial = 'D';
}

void exercici2() {
	printf("\n Exercici 2 \n");
	printf("------------------------------------\n");

	int num = 5;
	int mult;
	for (int i = 1; i <= 10; i++) {
		mult = i * num;
			printf("%d x %d = %d \n", i, num, mult);
	}
	printf("----------------------------------*/\n");
}

void exercici3() {
	printf("\n Exercici 3 \n");
	printf("----------------------------------\n");

	int num1 = 50;
	int num2 = 3;
	int operacion = num1 / num2;
	int residuo = num1 % num2;
	printf("Operacion -> %d / %d \n", num1, num2);
	printf("	Parte entera = %d \n", operacion);
	printf("	Residuo = %d \n", residuo);

	printf("----------------------------------*/\n");
}

void exercici4() {
	printf("\n Exercici 4 \n");
	printf("------------------------------------\n");

	int cont = 0;
	float i = 2;
	float j = 1;
	float operacion;
	while (cont < 100) {
		operacion = i / j;
		printf("%f ", operacion);
		i++;
		j++;
		cont++;
	}
	printf("\n");
	printf("----------------------------------*/\n");

}

void main() {
	printf("\n Alumne: Oscar Darriba Lopez \n");

	exercici1();
	exercici2();
	exercici3();
	exercici4();
	
	system("pause");
}