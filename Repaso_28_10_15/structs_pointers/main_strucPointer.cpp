//PENE
#include <stdio.h>
#include <cstdlib>
#include <math.h>
#include <string>
using namespace std;
//EXAMEN: Funciones, paso por referencia, structs, arrays

enum Specialization {Jedi, Sith, Bounty, Imperial};
enum Race {Human, Elf, Orc, Goblin};

struct Player {
	string name;
	Specialization spec;
	int life;
	int armour;
	int attack;
	bool isPoisoned;
	int poison; // How many turns of poison are left
	bool isLiving;
};

struct Enemy {
	string name;
	Race race;
	int life;
};

void fillPlayer(Player *jugadorActual, char* pName, Specialization pSpec, int pLife, int pArmour, int pAttack) {
	jugadorActual->name = pName;
	jugadorActual->spec = pSpec;
	jugadorActual->life = pLife;
	jugadorActual->armour = pArmour;
	jugadorActual->attack = pAttack;
	jugadorActual->isPoisoned = false;
	jugadorActual->poison = 0;
}

void substractHP(Player *jugadorActual, int damage) {
	if (jugadorActual->life <= 0) {
		jugadorActual->isLiving = false;
	}
	else {
		jugadorActual->life -= damage;
	}
	
}

void healHP(Player *jugadorActual, int healing) {
	if (jugadorActual->isLiving == true) {
		jugadorActual->life += healing;
	}
}

void checkIsLiving(Player *jugadorActual) {
	if (jugadorActual->isLiving) { // = true
		printf("%s is living", jugadorActual->name);
	}
	else {
		printf("%s RIP", jugadorActual->name);
	}
}
void main() {
	Player jugador[3];

	
	/*
	//P1
	jugadors[0].name = "Luke";
	jugadors[0].spec = Jedi;
	jugadors[0].life = 100;
	jugadors[0].armour = 30;
	jugadors[0].attack = 40;
	jugadors[0].isPoisoned = false;
	jugadors[0].isLiving = true;
	jugadors[0].poison = 0;
	//P2
	jugadors[1].name = "Bobba";
	jugadors[1].spec = Bounty;
	jugadors[1].life = 100;
	jugadors[1].armour = 50;
	jugadors[1].attack = 20;
	jugadors[1].isPoisoned = false;
	jugadors[1].isLiving = true;
	jugadors[1].poison = 0;
	//P3
	jugadors[2].name = "Vader";
	jugadors[2].spec = Sith;
	jugadors[2].life = 100;
	jugadors[2].armour = 40;
	jugadors[2].attack = 60;
	jugadors[2].isPoisoned = false;
	jugadors[2].isLiving = true;
	jugadors[2].poison = 0;
	*/
	
	
	Player* jugadorActual;
	jugadorActual = &jugador[0];	//mejor lo siguiente
	//jugadorActual = jugador;			//jugadores es un array, por lo que
										//es un puntero que apunta a la primera
										//pocisi�n del mismo.*/

	fillPlayer(jugadorActual, "Luke", Jedi, 100, 50, 20);

	substractHP(jugadorActual, 25);
	healHP(jugadorActual, 20);
	checkIsLiving(jugadorActual);
}



/*
void FillPlayer(Player *jugador,
string pName, Specialization pSpec,
int pLife, int pArmour, int pAttack)
{
jugador->name = pName;
jugador->spec = pSpec;
jugador->life = pLife;
jugador->armour = pArmour;
jugador->attack = pAttack;
jugador->isPoisoned = false;
jugador->poison = 0;

switch (pSpec)
{
case Barbarian:
jugador->attack = 300;
break;
case Cleric:
break;
case Sorcerer:

break;
case Burglar:
jugador->attack = 30;
break;
}
}
*/