#include <stdio.h>
#include <cstdlib>
#include <math.h>
#include <string>

#define MIDA 5

void division(int a, int b, int *entero, int *residuo) {
	*entero = a / b;
	*residuo = a % b;

}

void multiplyArray(int *array, int mida) {
	for (int i = 0; i < mida; i++) {
		array[i] = array[i] * 2;
		//*(array+i)
	}
}

void imprimirArray(int *array, int mida) {
	for (int i = 0; i < mida; i++) {
		printf("%d ", array[i]);
		//*(array+i)
	}
	printf("\n");
}

struct point {
	int x;
	int y;
};

float distance(point a, point b) {
	float p1 = b.x - a.x;
	float p2 = b.y - a.y;
	float distancia = sqrt(p1*p1 + p2*p2);
	
	return distancia;
}

float perimetro(float c1, float c2, float c3) {
	return c1 + c2 + c3;
}

float perimetreTriangle(point *vertexs) {
	float perimetre = 
		  distance(vertexs[0], vertexs[1]) +
		+ distance(vertexs[0], vertexs[2])
		+ distance(vertexs[1], vertexs[2]);

	return perimetre;
}

float LongestShortestSide(point *vertexs, float *longest, float *shortest) {

	float D1 = distance(vertexs[0], vertexs[1]);
	float D2 = distance(vertexs[0], vertexs[2]);
	float D3 = distance(vertexs[1], vertexs[2]);

	//LONG
	if (D1 > D2 && D1 > D3) {
		*longest = D1;
	}
	else if (D2 > D3) {
		*longest = D2;
	}
	else {
		*longest = D3;
	}

	//SHORT
	if (D1 < D2 && D1 < D3) {
		*shortest = D1;
	}
	else if (D2 < D3) {
		*shortest = D2;
	}
	else {
		*shortest = D3;
	}
	
}


void PuntMig(point *vertexs) {
	//CALCULAR PUNTO MEDIO DE UN LADO
}

void PoligonMig(point *vertexs, point* vertexsMig) {
	for (int i = 0; i < 3; i++) {

	}
}



void main() {
	/*
	/////////////EX1
	int a = 27;

	int b = 5;
	int entero;
	int residuo;
	division(a, b, &entero, &residuo);
	printf("%d %d \n", entero, residuo);
	*/ 
	////////////EX2
	/*int array[MIDA] = { 1,2,3,4,5 };

	imprimirArray(array, MIDA);
	multiplyArray(array, MIDA);
	imprimirArray(array, MIDA);

	system("pause");*/ 

	point points[3];
	points[0].x = 0;
	points[0].y = 0;

	points[1].x = 6;
	points[1].y = 2;

	points[2].x = 2;
	points[2].y = 2;

	float P1 = perimetreTriangle(points);
	printf("%f \n", P1);


	float llarg, curt;
	LongestShortestSide(points, &llarg, &curt);

	//Nuevo struct con los puntos medios de cada lado
	point vertexsMig[3];

	PoligonMig(points, vertexsMig);
	
	system("pause");
}