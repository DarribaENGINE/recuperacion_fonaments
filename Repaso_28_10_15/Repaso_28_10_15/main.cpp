#include <stdio.h>
#include <cstdlib>


//Operation
float operation(int a, int b, int c, int d) {
	float result = a + b * c / d;
	printf("Result ex1: %f \n", result);
	return result;
}

//Radev Math
void RadevMath() {
	int a = 3;
	int b = 4;
	int c = 5;
	int d = 0;
	printf("Result_1:\n a = %d\n b = %d\n c = %d\n d = %d \n", a, b, c, d);
	d = b * (++c);
	printf("Result_2:\n a = %d\n b = %d\n c = %d\n d = %d \n", a, b, c, d);
	a = d % a;
	printf("Result_3:\n a = %d\n b = %d\n c = %d\n d = %d \n", a, b, c, d);
	d = c;
	printf("Result_4:\n a = %d\n b = %d\n c = %d\n d = %d \n", a, b, c, d);
	d += c;
	printf("Result_5:\n a = %d\n b = %d\n c = %d\n d = %d \n", a, b, c, d);
	d *= (--a);
	printf("Result_6:\n a = %d\n b = %d\n c = %d\n d = %d \n", a, b, c, d);
	b *= b;
	printf("Result_7:\n a = %d\n b = %d\n c = %d\n d = %d \n", a, b, c, d);
	d -= a + b + c;
	printf("Result of RadevMath:\n a = %d\n b = %d\n c = %d\n d = %d \n", a, b, c, d);
}

//Convertir minuscula a mayuscula
char capitalLetter(char x) {
	char capitalChar;
	capitalChar = x - 32;
	printf("The capital letter is: %c \n", capitalChar);
	return capitalChar;
}

//Operators
/*
void operators(int a, int b, int c, int n) {
	bool andOp = a > b && a < c;
	printf("%s \n", andOp ? "true" : "false");

	bool orOp = a > b || a < c;
	printf("%s \n", andOp ? "true" : "false");

	bool done = true;
	done != done;

	a >= 0 && a <= 100;

	char answer; //Check answer is not y or Y
	answer != 'y' && answer != 'Y';
	
	int z = !a;
	printf("%d", z);

}*/

//Casting
void casting() {
	char a = 'h';
	int x = 4872;
	float y = 182.937;

	int char2int = a;
	printf("The character h when cast to an int gives value %d \n", char2int);

	float char2float = a;
	printf("The character when cast to an int gives value %d \n", char2float);

	char int2char = x;
	printf("The integer 4872 when cast to a char gives value %d \n", int2char);

	float int2float = x;
	printf("The integer 4872 when cast to a float gives value %d \n", int2float);

	int float2int = y;
	printf("The float 182.937 when cast to an int gives value %d \n", float2int);

	char float2char = y;
	printf("The float 182.937 when cast to a char gives value %d \n", float2char);

}

void fizzbuzz() {
	for (int i = 1; i < 100; i++) {
		if (i % 3 == 0 && i % 5 == 0) {
			printf("%d FIZZBUZZ \n", i);
		}
		else if (i % 3 == 0) {
			printf("%d FIZZ \n", i);
		}
		else if (i % 5 == 0) {
			printf("%d BUZZ \n", i);
		}
		else {
			printf("%d \n", i);
		}
	}
}

void bucleWhile() {
	int val = 0;
	while (val < 10) {
		printf("%d", val++);
	}
}

void asteriscos() {
	for (int i = 1; i <= 5; i++) {
		for (int j = 1; j <= i; j++) {
			printf("*");
		}
		printf("\n");
	}
	
}

void asteriscos2() {
	for (int i = 6; i > 0; i--) {
		for (int j = 0; j < i; j++) {
			printf("*");
		}
		printf("\n");
	}
}

void factorial() {
	int max = 5;
	int acum = 1;
	for (int i = 2; i <= max; i++) {
		acum *= i;
		
	}
	printf("%d", acum);
}

void asteriscos3() {
	for (int i = 5; i >= 1; i--) {
		for (int j = 1; j <= i; j++) {
			printf("%d",j);
		}
		printf("\n");
	}

}

void exercici3() {
	int max = 100;
	for (int i = 2; i < max; i++) {
		if (max % i == 0) {
			printf("%d \n", i);
		}
	}
}

//MAIN
void main() {
	//operation(1, 1, 1, 1);	//R: 2
	//RadevMath();
	//capitalLetter('b');
	//**********************
	//Operators
	//operators(20, 5, 6, 80);
	//casting();
	//**********************
	//fizzbuzz();
	//bucleWhile();

	//asteriscos();
	//asteriscos2();
	//asteriscos3();
	//factorial();
	exercici3();
	printf("\n \n fsociety \n \n");
	system("pause");
}

