#include <stdio.h>
#include <cstdlib>
#include <time.h>
#include <iostream>


#define MIDA 5

float calculateAverage(int* array, int mida) {
	float acum = 0;
	float avg = 0;
	for (int i = 0; i < mida; i++) {
		acum += array[i];

	}
	avg = acum / mida;
	printf("La medida del array es: %f \n", avg);
	return avg;
}

void randomizeArray(int* array, int mida) {

	for (int i = 0; i < mida; i++) {
		array[i] = rand() % 101;
	}
}


void main() {

	time_t t;
	time(&t);
	srand(t);

	int mida;
	std::cout << "Inserta la medida del Array" << std::endl;
	std::cin >> mida;


	int* numbers = (int*)malloc(mida*sizeof(int));
	calculateAverage(numbers, mida);
	//randomizeArray(numbers, mida);

	free(numbers);
	system("pause");



	
}