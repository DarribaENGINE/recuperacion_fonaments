#include <stdio.h>
#include <cstdlib>
#define _USE_MATH_DEFINES
#include <math.h>
#include <string>
#define NUM 5

using namespace std;

void abecedario() {
	char letters[26];
	for (int i = 0; i <= 25; i++) {
		letters[i] = 65 + i;
		printf("%c ", letters[i]);
	}
}

float CircleArea(float radius) {
	float area = M_PI * radius * radius;
	printf("Area = %f \n", area);
	return area;
}

int triangular(int num) {
	int acum = 0;
	for (int i = 1; i <= num; i++) {
		acum = acum + i;
	}
	printf("Triangular = %d \n", acum);
	return acum;
}

void print_addr(int *a) {
	printf("Addres of int a = %x \n", a);
}



void main() {
	//abecedario();
	//CircleArea(NUM);
	//triangular(NUM);
	int val = 5;


	print_addr(&val);
	system("pause");
}